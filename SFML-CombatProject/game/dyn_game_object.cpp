#include "dyn_game_object.hpp"

void pixl_sf::game::DynamicGameObject::update(sf::RenderWindow* wnd)
{
	if (updateFun) updateFun(wnd);
}

void pixl_sf::game::DynamicGameObject::draw(sf::RenderWindow* wnd)
{
	if (drawFun) drawFun(wnd);
}
