#pragma once
#include <string>
#include <vector>
#include <filesystem>
#include <regex>
#include "game_object.hpp"
#include "resources.hpp"
#include <SFML/Graphics/RectangleShape.hpp>


namespace pixl_sf::game
{
	class AnimationSequence : public GameObject
	{
	public:


		AnimationSequence() : itr( m_textures.begin()) {
			
		}


		AnimationSequence(const AnimationSequence& other)
			: GameObject(other),
			  m_path(other.m_path),
			  m_base(other.m_base),
			  m_textures(other.m_textures),
			  itr(other.itr)
		{
		}

		AnimationSequence(AnimationSequence&& other) noexcept
			: GameObject(std::move(other)),
		      m_path(std::move(other.m_path)),
			  m_base(std::move(other.m_base)),
			  m_textures(std::move(other.m_textures)),
			  itr(std::move(other.itr))
		{
		}

		AnimationSequence& operator=(const AnimationSequence& other)
		{
			if (this == &other)
				return *this;
			GameObject::operator =(other);
			m_path = other.m_path;
			m_base = other.m_base;
			m_textures = other.m_textures;
			itr = other.itr;
			return *this;
		}

		AnimationSequence& operator=(AnimationSequence&& other) noexcept
		{
			if (this == &other)
				return *this;
			GameObject::operator =(std::move(other));
			m_path = std::move(other.m_path);
			m_base = std::move(other.m_base);
			m_textures = std::move(other.m_textures);
			itr = std::move(other.itr);
			return *this;
		}

		bool fuzzy_same(const std::string& cs)
		{
			return cs == m_path;
		}

		AnimationSequence(const std::string& path,int expected_size = 150)
		{
			// set path
			m_path = path;
			//resize to expected size of sequence elements
			m_textures.resize(expected_size);

			namespace fs = std::filesystem;

			
			fs::directory_iterator directory(path);

			int max_idx = 0;


			//iterate over directory
			for(auto & file : directory)
			{

				//check if the file is just a default file (no symlinks etc...)
				if(file.is_regular_file())
				{
					//regex ^.+([0-9]+)\.[A-z]+$
					std::regex matcher(R"(^.+([0-9]{4})\.[A-z]+$)");
					std::smatch matches;
					std::string filename = fs::path(file).filename().string();


					//check if the file matches the expected criteria
					if(std::regex_search(filename,matches,matcher))
					{

						//check if the match-size if correct
						if (matches.size() != 2) continue;
						{

							//extract the index size
							const size_t idx = stoul(matches[1].str());

							//check if this is the current max_idx
							if (idx > max_idx) max_idx = idx;

							//check if we need to resize the vector
							if(m_textures.size() < idx)
							{
								m_textures.resize(idx + 1);
							}

							//load the texture
							m_textures[idx] = &resources::ResourceManager::loadTexture(path + filename);
						}
					}
					
				}
			}

			m_textures.resize(1+max_idx);
			itr = m_textures.begin();
		}

		sf::RectangleShape& getBaseShape() { return m_base; }

		
		void update(sf::RenderWindow* wnd) override
		{
			if (m_path.empty()) return;
			if ((m_cylces += m_deltaClock.restart().asSeconds()) > 1.f/30.f)
			{
				++itr;
				m_cylces = 0;
			}
			if (itr == m_textures.end())
				itr = m_textures.begin();
		}
		void draw(sf::RenderWindow* wnd) override
		{
			if (m_path.empty()) return;

			m_base.setTexture(*itr);
			wnd->draw(m_base);
		}

	private:
		float m_cylces = 0.f;
		sf::Clock m_deltaClock;
		std::string m_path;
		sf::RectangleShape m_base;
		std::vector<sf::Texture*> m_textures;
		decltype(m_textures)::iterator itr;
	};
}
