#pragma once
#include <cstdint>
#include <functional>
#include "game_object.hpp"
#include <map>
#include <utility>

#define stransition(id)  return ::pixl_sf::logic::StateMachine::YieldTo(::pixl_sf::logic::StateMachine::YIELD_TYPE::TRANSITION,id)
#define sstay() return ::pixl_sf::logic::StateMachine::YieldTo(::pixl_sf::logic::StateMachine::YIELD_TYPE::STAY,0xFF)
constexpr uint8_t TRANSITION_ID_STAY = 0xFF;

namespace pixl_sf::logic
{
	class StateMachine : public game::GameObject
	{
	public:
		using transition_id_t = std::uint8_t;
	
		enum YIELD_TYPE
		{
			STAY,
			TRANSITION
		};
		struct YieldTo
		{
			YIELD_TYPE type;
			transition_id_t to;

			YieldTo( YIELD_TYPE t, transition_id_t whereto) : type(t), to(whereto){}
		};
		using state_func = std::function <YieldTo(transition_id_t)>;

		
		void addTransition(transition_id_t id,state_func f);

		void update(sf::RenderWindow* wnd) override;
		void draw(sf::RenderWindow* wnd) override;
		
	private:
		std::map<transition_id_t, state_func> m_states;
		transition_id_t m_active_state = 0;
		transition_id_t m_yield_state = 0;
	};

	inline void StateMachine::addTransition(transition_id_t id,state_func f)
	{
		m_states[id] = std::move(f);
	}

	inline void StateMachine::update(sf::RenderWindow* wnd)
	{
		YieldTo yielded = m_states[m_active_state](m_yield_state);
		if(yielded.type == TRANSITION)
		{
			m_yield_state = m_active_state;
			m_active_state = yielded.to;
		}
	}

	inline void StateMachine::draw(sf::RenderWindow* wnd)
	{
	}

}
