#pragma once
#include <array>
#include <vector>
#include <fstream>
#include <nlohmann/json.hpp>

#include "attack.hpp"
#include <iostream>


namespace  pixl_sf::game {


	struct Fighter
	{
		std::string config_file;
		std::string pretty_name;
		
		std::array<Effect, eff2idx(Effect::SIZE)> active_effects;

		int health;
		
		std::vector<Attack> attacks;
		sf::Texture* img_small;
		std::string animation_sequence;
		
		const static int MAX_HEALTH = 100;
				
	};

	struct FighterFactory
	{
		static Fighter* loadFighter(Fighter* f)
		{
			using json = nlohmann::json;

			//open config
			std::ifstream infile(f->config_file);
			if (infile.bad()) return nullptr;

			//parse config
			json j;
			infile >> j;

			//set name
			f->pretty_name = j["pretty_name"];

			if(j["img_small"].is_string())
			{
				f->img_small = &resources::ResourceManager::loadTexture(j["img_small"].get<std::string>());
			}
			if(j["animation_sequence"].is_string())
			{
				f->animation_sequence = j["animation_sequence"].get<std::string>();
			}
			
			//clear effects
			f->active_effects = std::array<Effect, eff2idx(Effect::SIZE)>{Effect{ Effect::NONE,0,0 }};

			try{
				//load attacks
				if (j["attacks"].is_array())
					for (const auto& attackfile : j["attacks"])
						f->attacks.push_back(loadAttack(attackfile.get<std::string>()));
			} catch (std::runtime_error&)
			{
				return nullptr;
			}

			//finally return self
			return f;
		}
		static std::shared_ptr<Fighter> loadFighter(const std::string& config )
		{
			auto f = std::make_shared<Fighter>();

			f->health = Fighter::MAX_HEALTH;
			f->config_file = config;
			auto res = loadFighter(f.get());

			
			return f.get() == res ? f : nullptr;
		}
		
		static Attack loadAttack(std::string attack_file)
		{
			using json = nlohmann::json;

			
			//open config
			std::ifstream infile(attack_file);
			if (infile.bad()) throw std::runtime_error("Could not open configuration file for attack: " + attack_file);

			//parse
			json j;
			infile >> j;

			//create attack 
			Attack a{};
			a.name = j["name"].get<std::string>();
			a.attack_power = j["attack_power"].get<int>();


			//iterate over effects
			if(j["effects"].is_array()){
				for (const auto& element : j["effects"])
				{
					//parse type
					auto type = effect_name2effect_type(element["type"].get<std::string>());
					if (type == Effect::NONE) {
						std::cout << "Warning encountered unrecognized effect " + element["type"].get<std::string>();
						continue;
					}

					//construct effect
					a.effects[eff2idx(type)] = Effect{
							type,
							element["duration"],
							element["strength"]
					};
				}
			}
			return a;
		}
	};


	
}
