#pragma once
#include <string>
#include <array>

namespace pixl_sf::game {

	struct Effect {
		enum EffectType : uint8_t
		{
			NONE = 0,
			POISON = 1,
			FIRE = 2,
			WEAKNESS = 4,
			SIZE = 0XFF
		} type;
		int duration;
		int strength;
	};



	inline Effect::EffectType effect_name2effect_type(const std::string& name)
	{
		if (name == "POISON") return Effect::POISON;
		if (name == "FIRE") return  Effect::FIRE;
		if (name == "WEAKNESS") return Effect::WEAKNESS;
		return  Effect::NONE;
	}
	
	[[nodiscard]] static constexpr const int eff2idx(Effect::EffectType e)
	{
		return	e == Effect::NONE ? -1 :
				e == Effect::POISON ? 0 :
				e == Effect::FIRE ? 1 :
				e == Effect::WEAKNESS ? 2 :
				3;
	}
	
	struct Attack
	{
		std::string name;

		int attack_power;
		
		std::array<Effect, eff2idx(Effect::SIZE)>  effects;
		
	};

	static const Attack empty_attack = { "",0,{ {{Effect::NONE,0,0}} } };
}