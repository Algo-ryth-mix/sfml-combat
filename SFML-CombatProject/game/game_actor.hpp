#pragma once
#include "fighter.hpp"
#include "game_object.hpp"
#include <SFML/Graphics/Text.hpp>
#include "resources.hpp"
#include "animation_sequence.hpp"

namespace pixl_sf::game {

	class GameActor : public  GameObject
	{
	public:

		//add a new fighter to the list of available fighters
		Fighter* addFighter(const std::shared_ptr<Fighter>& f)
		{
			m_fighters.insert(m_fighters.begin(), f);
			return f.get();
		}

		//sets the name of the Actor
		void setName(const std::string& name)
		{
			m_name = name;
		}

		void setLayoutInverted(bool inv)
		{
			m_inverted = inv;
		}

		void selectFighter(size_t num)
		{
			if (num > getRemainingFighters()) return;

			m_activeFighter = m_fighters.begin();
			std::advance(m_activeFighter, num);
		}
		
		
		//checks how many fighters are still available
		[[nodiscard]] size_t getRemainingFighters() const
		{
			return m_fighters.size();
		}

		//gets the current fighter
		[[nodiscard]] std::shared_ptr<Fighter> getFighter() const
		{
			if(m_activeFighter != m_fighters.end())
			{
				return *m_activeFighter;
			}
			return nullptr;		
		}

		//sets where to start drawing the overlay
		void setDrawPos(sf::Vector2f pos)
		{
			m_drawPos = pos;
		}

		void update(sf::RenderWindow* wnd) override
		{

			m_fighter_sprite.update(wnd);
			
			//if there are no fighters remaining we can quit
			if (getRemainingFighters() == 0) return;

			
			//check if the current fighter is invalid 
			if(m_activeFighter == m_fighters.end())
			{
				//set the current fighter to a new fighter
				m_activeFighter = m_fighters.begin();
			}

			
			auto& fighter_ptr = *m_activeFighter;

			//check if the current fighter is dead
			if(fighter_ptr->health <= 0)
			{
				//delete the fighter
				m_fighters.remove(fighter_ptr);

				//set the active fighter to an invalid position
				m_activeFighter = m_fighters.end();
			}
		}
		
		void draw(sf::RenderWindow* wnd) override
		{

			static const auto wnx = wnd->getSize().x;
			
			//draw name
			sf::Text name_text(m_name, resources::ResourceManager::getDefaultFont());
			name_text.setPosition(m_drawPos + sf::Vector2f{PADDING,PADDING});
			name_text.setFillColor({ 0,0,0 });
			wnd->draw(name_text);

			//draw list of available monsters

			//draw the rect containing all items of the list
			sf::RectangleShape listRect;
			auto listPos = m_drawPos;
			listPos.y += LIST_OFFSET_TOP + PADDING;

			if(!m_inverted){
				listPos.x = (wnx - LIST_SIZE.x) - PADDING;	
			} else {
				listPos.x += PADDING;
			}

			listRect.setPosition(listPos);
			listRect.setSize(LIST_SIZE);
			listRect.setOutlineColor({ 0,0,0 });
			listRect.setOutlineThickness(2);

			int offs = 0;
			wnd->draw(listRect);

			for (auto& fighter : m_fighters)
			{
				//cannot draw more than 6 items
				if (offs >= 6) break;

				sf::RectangleShape listEntry;

				//Draw the box around our entry
				sf::Vector2f localPos = { 0,static_cast<float>(offs++) * LIST_ENTRY_SIZE.y };
				listEntry.setPosition(listPos + localPos);
				listEntry.setSize(LIST_ENTRY_SIZE);
				listEntry.setOutlineColor({ 0,0,0 });
				listEntry.setOutlineThickness(-4);

				if(m_activeFighter != m_fighters.end())
				//if it is the active fighter color the box green
				if(*m_activeFighter == fighter)
				{
					listEntry.setOutlineColor({ 25,255,120 });
				}
				
				wnd->draw(listEntry);


				//draw the name of the fighter
				sf::Text fighterNameText(fighter->pretty_name,resources::ResourceManager::getDefaultFont(),16);
				localPos.y += PADDING;
				localPos.x += PADDING;
				fighterNameText.setPosition(listPos + localPos);
				fighterNameText.setFillColor({ 0,0,0 });
				wnd->draw(fighterNameText);

				localPos.y += PADDING * 3;


				//draw the healthbar of the fighter
				sf::RectangleShape healthBarBase(LIST_ENTRY_HEALTH_BAR_SIZE);
				sf::RectangleShape healthBarIndicator(LIST_ENTRY_HEALTH_BAR_SIZE - sf::Vector2f{
					static_cast<float>(Fighter::MAX_HEALTH - fighter->health) /Fighter::MAX_HEALTH * LIST_ENTRY_HEALTH_BAR_SIZE.x,
					0});
				healthBarBase.setFillColor({ 255,0,0 });
				healthBarIndicator.setFillColor({ 0,255,0 });
				healthBarBase.setPosition(listPos + localPos);
				healthBarIndicator.setPosition(listPos + localPos);

				wnd->draw(healthBarBase);
				wnd->draw(healthBarIndicator);


				//Draw the preview fighter
				sf::RectangleShape previewPic(PREVIEW_FIGHTER_SIZE);
				previewPic.setTexture(fighter.get()->img_small);
				previewPic.setPosition(listPos + sf::Vector2f(100,static_cast<float>(offs * LIST_ENTRY_SIZE.y) - 80));
				wnd->draw(previewPic);
				
			}

			if (m_activeFighter != m_fighters.end()) {
				//draw the healthbar of the fighter

				sf::RectangleShape activeHealthBarBase(ACTIVE_HEALTH_BAR_SIZE);
				sf::RectangleShape activeHealthBarIndicator(ACTIVE_HEALTH_BAR_SIZE - sf::Vector2f{ 
					static_cast<float>(Fighter::MAX_HEALTH - m_activeFighter->get()->health) / 100.0f * ACTIVE_HEALTH_BAR_SIZE.x,
					0
				});
				activeHealthBarBase.setFillColor({ 255,0,0 });
				activeHealthBarIndicator.setFillColor({ 0,255,0 });
				activeHealthBarBase.setPosition(m_drawPos + ACTIVE_HEALTH_BAR_OFFSET);
				activeHealthBarIndicator.setPosition(m_drawPos + ACTIVE_HEALTH_BAR_OFFSET);

				wnd->draw(activeHealthBarBase);
				wnd->draw(activeHealthBarIndicator);
				
				//draw the fighter
				if (!m_activeFighter->get()->animation_sequence.empty() && !m_fighter_sprite.fuzzy_same(m_activeFighter->get()->animation_sequence))
				{
					m_fighter_sprite = AnimationSequence(m_activeFighter->get()->animation_sequence);
				}

				m_fighter_sprite.getBaseShape().setSize(FIGHTER_SIZE);
				if (m_inverted)
				{
					m_fighter_sprite.getBaseShape().setPosition({ (float)wnx / 2.f - FIGHTER_X_SPACING - FIGHTER_SIZE.x / 2.f,100 });
				}
				else
				{
					m_fighter_sprite.getBaseShape().setPosition({ (float)wnx / 2.f + FIGHTER_X_SPACING - FIGHTER_SIZE.x / 2.f,100 });
				}

				m_fighter_sprite.draw(wnd);
			}
		}

		//gets the name of the actors current fighter 
		std::string getNameOfFighter(size_t size)
		{
			auto i = m_fighters.begin();
			std::advance(i, size);
			if(i != m_fighters.end())
			{
				return i->get()->pretty_name;
			}
			return "";
		}


		//Resets the Actor
		void clear()
		{
			m_fighters.clear();
			m_activeFighter = m_fighters.begin();
			
		}
	private:

		AnimationSequence m_fighter_sprite;
		const sf::Vector2f LIST_SIZE  { 200,360 };
		const float LIST_OFFSET_TOP = 40;
		const float PADDING = 10;
		const sf::Vector2f LIST_ENTRY_SIZE = { 200,60 };
		const sf::Vector2f LIST_ENTRY_HEALTH_BAR_SIZE = { 100,10 };

		const sf::Vector2f ACTIVE_HEALTH_BAR_OFFSET = { 200,PADDING};
		
		const sf::Vector2f ACTIVE_HEALTH_BAR_SIZE = { 200,30 };

		const sf::Vector2f PREVIEW_FIGHTER_SIZE = { 100,100 };
		const sf::Vector2f FIGHTER_SIZE = { 400,400 };
		const float FIGHTER_X_SPACING = 200;
		
		
		bool m_inverted = false;
		std::string m_name = "Opponent";
		
		std::list<std::shared_ptr<Fighter>> m_fighters;
		decltype(m_fighters)::iterator m_activeFighter = m_fighters.end();
		sf::Vector2f m_drawPos { 0.0f,0.0f };
		
		
	};

}
