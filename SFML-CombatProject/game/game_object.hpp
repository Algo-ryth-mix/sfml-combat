#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

namespace pixl_sf::game
{

	class GameObject
	{
	public:
		virtual ~GameObject() = default;
		virtual void update(sf::RenderWindow* wnd) = 0;
		virtual void draw(sf::RenderWindow* wnd) = 0;
	};
}
