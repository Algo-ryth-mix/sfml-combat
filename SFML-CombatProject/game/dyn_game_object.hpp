#pragma once
#include "game/game_object.hpp"
#include <functional>


namespace pixl_sf::game {



	class DynamicGameObject : public GameObject {

		using func_t = std::function<void(sf::RenderWindow*)>;

		func_t updateFun;
		func_t drawFun;

	public:
		void setUpdate(func_t f) {
			updateFun = f;
		}

		void setDraw(func_t f) {
			drawFun = f;
		}

		virtual void update(sf::RenderWindow* wnd) override;
		virtual void draw(sf::RenderWindow* wnd) override;

	};
}