#include "game/scenes.hpp"

namespace pixl_sf::game
{
	void Scene::update(sf::RenderWindow* wnd)
	{
		for (auto game_object : m_gameObjects)
		{
			game_object->update(wnd);
		}
	}

	void Scene::draw(sf::RenderWindow* wnd)
	{
		for (auto game_object : m_gameObjects)
		{
			game_object->draw(wnd);
		}
	}

	Scene* SceneManager::addScene(const std::string& name)
	{
		if (m_sceneRegistry.find(name) != m_sceneRegistry.end())
			return nullptr;

		const auto itr = m_sceneRegistry.emplace(name, std::make_shared<Scene>(name));
		return itr.first->second.get();
	}

	Scene* SceneManager::getScene(const std::string& name)
	{
		if (m_sceneRegistry.find(name) == m_sceneRegistry.end()) return nullptr;
		return m_sceneRegistry.at(name).get();
	}

	void SceneManager::pushScene(const std::string& name)
	{
		if (m_sceneRegistry.find(name) == m_sceneRegistry.end()) return;
		const auto scene = m_sceneRegistry.at(name);
		m_activeSceneStack.push(scene);
	}

	void SceneManager::popScene()
	{
		m_activeSceneStack.pop();
	}

	bool SceneManager::is_active(const std::string& name)
	{
		return m_activeSceneStack.top()->getName() == name;
	}

	void SceneManager::update(sf::RenderWindow* window)
	{
		if (!m_activeSceneStack.empty())
			m_activeSceneStack.top()->update(window);
	}

	void SceneManager::draw(sf::RenderWindow* window)
	{
		if (!m_activeSceneStack.empty())
			m_activeSceneStack.top()->draw(window);
	}
}
