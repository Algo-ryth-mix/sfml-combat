#pragma once
#include "game/game_object.hpp"
#include <map>
#include <stack>
#include <utility>

namespace pixl_sf::game
{
	class Scene : public GameObject
	{

	public:

		Scene(std::string name) : m_name(std::move(name)) {}

		Scene(const Scene& other) = default;
		Scene(Scene&& other) noexcept = default;
		Scene& operator=(const Scene& other) = default;
		Scene& operator=(Scene&& other) noexcept = default;

		friend bool operator==(const Scene& lhs, const Scene& rhs)
		{
			return lhs.m_name == rhs.m_name;
		}
		friend bool operator!=(const Scene& lhs, const Scene& rhs)
		{
			return !(lhs == rhs);
		}

		[[nodiscard]] std::string getName() const { return m_name; }


		//the anti-lambda
		template<class T,class... Varargs,class O = typename std::enable_if<std::is_base_of<GameObject,T>::value,T>::type>
		O* addGameObject(Varargs&&... v)
		{
			//create game object
			if constexpr (sizeof... (v) != 0)
				m_gameObjects.push_back(std::shared_ptr<GameObject>(new O(v...)));
			else
				m_gameObjects.push_back(std::shared_ptr<GameObject>(new O));


			//get game object
			const std::shared_ptr<GameObject> o = m_gameObjects.back();

			//cast and return game object
			return static_cast<O*>(o.get());
		}

		void update(sf::RenderWindow* wnd) override;
		void draw(sf::RenderWindow* wnd) override;

	private:
		std::vector<std::shared_ptr<GameObject>> m_gameObjects;
		std::string m_name;
	};


	class SceneManager : public GameObject
	{
	public:
		Scene* addScene(const std::string& name);
		Scene* getScene(const std::string& name);
		void pushScene(const std::string& name);
		void popScene();
		bool is_active(const std::string& name);

		void update(sf::RenderWindow* window) override;
		void draw(sf::RenderWindow* window) override;


	private:
		std::map<std::string, std::shared_ptr<Scene>> m_sceneRegistry;
		std::stack<std::shared_ptr<Scene>> m_activeSceneStack;
	};
}
