#pragma once
#include "button.hpp"
#include <SFML/Window/Mouse.hpp>

namespace pixl_sf::ui
{
	class ButtonManager : public game::GameObject
	{
	public:
		void addButton(std::shared_ptr<Button> button)
		{
			m_buttons.push_back(button);
		}

		void clear()
		{
			m_buttons.clear();
		}
		
		void update(sf::RenderWindow* wnd) override
		{
			for (const auto& button : m_buttons)
			{
				button->onMouseEvent(sf::Mouse::getPosition(*wnd), sf::Mouse::isButtonPressed(sf::Mouse::Left));
				button->update(wnd);
			}
		}

		void draw(sf::RenderWindow* wnd) override
		{
			for(const auto& button : m_buttons)
			{
				button->draw(wnd);
			}
		}

		
	private:
		std::vector<std::shared_ptr<Button>> m_buttons;
		
	};
}
