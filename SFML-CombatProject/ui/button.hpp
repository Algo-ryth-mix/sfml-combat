#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include "resources.hpp"
#include <functional>
#include <utility>
#include <memory>
#include <string>
#include "game/game_object.hpp"


namespace pixl_sf::ui
{

	class Button : public game::GameObject
	{
	public:

		enum ButtonEvent
		{
			ON_HOVER,
			ON_PRESS,
			ON_RELEASE
		};

		
		Button()
		{
			m_background_shape.setFillColor(sf::Color::White);
			m_background_shape.setOutlineColor(sf::Color::Black);
			m_foreground_shape.setFillColor(sf::Color{ 200,200,200, });
			m_text_shape.setFont(resources::ResourceManager::getDefaultFont());
			m_text_shape.setFillColor(sf::Color{ 255,0,0 });
			m_text_shape.setCharacterSize(16);

		}

		void onEvent(std::function<void(ButtonEvent event,Button* button)> func)
		{
			m_eventCallback = std::move(func);
		}

		void onMouseEvent(sf::Vector2i mouse,bool press)
		{
			if(
				mouse.x >= m_position.x &&
				mouse.y >= m_position.y &&
				mouse.x <= m_position.x + m_size.x &&
				mouse.y <= m_position.y + m_size.y)
			{
				m_eventCallback(ON_HOVER, this);
				m_foreground_shape.setFillColor(sf::Color{ 125,125,125 });


				static bool once = false;
				if(press)
				{
					m_foreground_shape.setFillColor(sf::Color{ 50,50,50 });
					if(!once)
					{
						once = true;
						m_eventCallback(ON_PRESS,this);
					}
				} else if(once)
				{
					m_eventCallback(ON_RELEASE, this);
					once = false;
				}


				
			} else {
				m_foreground_shape.setFillColor(sf::Color{ 200,200,200 });
			}

		}
		
		
		void setPosition(const sf::Vector2f& pos)
		{
			m_position = pos;
			m_background_shape.setPosition(pos);
			m_foreground_shape.setPosition(pos + sf::Vector2f{ 2.f,2.f });
			m_text_shape.setPosition(pos);
		}

		void setSize(const sf::Vector2f& size)
		{
			m_size = size;
			m_background_shape.setSize(size);
			m_foreground_shape.setSize(size - sf::Vector2f{ 4.f,4.f });
		}

		void setText(const std::string& text)
		{
			m_text_shape.setString(text);
		}

		void update(sf::RenderWindow* wnd) override
		{
			m_text_shape.setPosition(m_position + m_size / 2.f);
			const sf::FloatRect textRect = m_text_shape.getLocalBounds();
			m_text_shape.setOrigin(textRect.left + textRect.width / 2.0f,
				textRect.top + textRect.height / 2.0f);
		}


		void draw(sf::RenderWindow* window) override
		{

			window->draw(m_background_shape);
			window->draw(m_foreground_shape);
			window->draw(m_text_shape);
		}
	private:
		sf::Vector2f m_position;
		sf::Vector2f m_size;


		std::function<void(ButtonEvent, Button*)> m_eventCallback = [](ButtonEvent,Button*){};
		sf::RectangleShape m_background_shape;
		sf::RectangleShape m_foreground_shape;
		sf::Text   m_text_shape;
		
	};


	inline std::shared_ptr<Button> make_button() {
		return std::make_shared<Button>();
	}

}
