#pragma once
#include <SFML/Graphics/Font.hpp>
#include <unordered_map>

namespace pixl_sf::resources
{
	class ResourceManager
	{
	public:
		static sf::Font& getDefaultFont()
		{
			static sf::Font s;
			static bool loaded = false;
			if(!loaded)
			{
				s.loadFromFile("default.ttf");
				loaded = true;
			}

			return s;
		}

		static sf::Texture& loadTexture(const std::string& path)
		{
			static std::unordered_map<std::string, sf::Texture> m_textures;
			if (m_textures.find(path) != m_textures.end()) return m_textures.at(path);

			auto i = m_textures.emplace(path, sf::Texture{});
			if(!i.first->second.loadFromFile(path))
			{
				throw std::runtime_error("Sprite File: " + path + " does not exist!");
			}
			return i.first->second;
		}
		
	};
}
