
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "ui/button.hpp"
#include "game/scenes.hpp"
#include "game/fighter.hpp"
#include "game/game_actor.hpp"
#include "game/statemachine.hpp"
#include "ui/button_manager.hpp"
#include <random>
#include <utility>
#include <thread>
#include <argh/argh.h>
#include "game/dyn_game_object.hpp"
#include <iomanip>
#include "game/animation_sequence.hpp"

const const char* const LEADERBOARD_FILE = "leaderboard.cmgt";


class GameStateMachine : public pixl_sf::logic::StateMachine
{
public:

	StateMachine state_machine;
	using state = transition_id_t;

	//only appropriate
	using ytho = YieldTo;
	
	GameStateMachine(pixl_sf::game::GameActor* player,pixl_sf::game::GameActor* enemy,
		pixl_sf::ui::ButtonManager* button_manager,const int& diff) :
		m_player(player),
		m_enemy(enemy),
		m_button_manager(button_manager),
		// NOLINT(cert-msc32-c)
		device(time(nullptr)),
	    m_difficulty_ref(diff),
	    m_status_text("something happened!",pixl_sf::resources::ResourceManager::getDefaultFont())
	{

		//fit status text
		m_status_text.setPosition({ 50,660 });
		m_status_text.setFillColor({ 0,0,0 });
		
		std::uniform_int_distribution<int> distribution(0, 100);

		#pragma region buttons
		//create the buttons
		for (auto& button : m_buttons)
		{
			button = pixl_sf::ui::make_button();
			button->setSize(BUTTON_SIZE);
			
			m_button_manager->addButton(button);
		}


		auto choose_fighter_button = pixl_sf::ui::make_button();
		choose_fighter_button->setSize(BUTTON_SIZE);
		choose_fighter_button->setText("Select Fighter");
		choose_fighter_button->setPosition({ 40,590 });
		choose_fighter_button->onEvent([&](auto evnt, auto)
		{
			if(evnt == pixl_sf::ui::Button::ON_PRESS)
				m_choose_fighter_button_pressed = true;
		});
		m_button_manager->addButton(choose_fighter_button);

		auto choose_attack_button = pixl_sf::ui::make_button();
		choose_attack_button->setSize(BUTTON_SIZE);
		choose_attack_button->setText("select Attack");
		choose_attack_button->setPosition({40,500});
		choose_attack_button->onEvent([&](auto evnt, auto)
		{
			if (evnt == pixl_sf::ui::Button::ON_PRESS)
				m_choose_fighter_button_pressed = false;
		});
		m_button_manager->addButton(choose_attack_button);

		
		//create the button callbacks
		for(size_t i = 0;i < m_buttons.size();++i)
		{
			
			m_buttons[i]->onEvent([&,i](auto evt, auto btn)
			{
				if(evt == pixl_sf::ui::Button::ON_PRESS){
					for (auto& button_p : m_button_presses) button_p = false;

					//on deletion the sfml callback might call into GameStateMachine after the destructor has been called
					//in this case i is not valid anymore as this-> == nullptr (this-> is lambda-state)
					//we just check that it is within bounds
					m_button_presses[i<m_buttons.size()?i:0] = true;
				}
			});
		}
		#pragma endregion


		//ye ol statemachine
		addTransition(PLAYER_CHOOSE_ATTACK_STATE, [&](state) -> ytho
		{
			//get the current button states
			auto buttons = listenButtonPress();

			if (m_choose_fighter_button_pressed) stransition(PLAYER_CHOOSE_FIGHTER_STATE);

			for(auto& b :m_buttons)
			{
				b->setText("");
			}
			for(size_t i = 0 ;i < std::min(m_player->getFighter()->attacks.size(),m_buttons.size());++i)
			{
				m_buttons[i]->setText(m_player->getFighter()->attacks[i].name);
			}
			

			//check if any of the buttons is pressed
			if(const auto itr = std::max_element(buttons.begin(), buttons.end());*itr)
			{
				//check which buttons was pressed
				ptrdiff_t dist = std::distance(	buttons.begin(), itr);

				//select the attack
				if (static_cast<size_t>(dist) < m_player->getFighter()->attacks.size())
					m_player_attack = m_player->getFighter()->attacks[dist];
				else
					sstay();

				//transition to take damage state
				stransition(ENEMY_TAKE_DAMAGE_STATE);
			}

			//stay
			sstay();
		});
		addTransition(ENEMY_TAKE_DAMAGE_STATE, [&](state) -> ytho
		{
			//do damage calculation
			int weakness_mod = 0;

			//extract weakness
			auto& weakness = m_enemy->getFighter()->active_effects[eff2idx(pixl_sf::game::Effect::WEAKNESS)];

			//check if weakness exists
			if (weakness.type != pixl_sf::game::Effect::NONE || weakness.duration != 0){
					weakness_mod = weakness.strength;
					weakness.duration--;
			}


			m_status_text.setString("You attacked the Enemy with: " + std::to_string(m_player_attack.attack_power * (weakness_mod + 1)) + " damage");
			
			//reduce health
			m_enemy->getFighter()->health -= m_player_attack.attack_power * (weakness_mod + 1); //oops ... fixed bug that with a weakness of 0 nothing does damage

			//add effects
			for(const auto& effect : m_player_attack.effects)
			{
				if(effect.type != pixl_sf::game::Effect::NONE)
					m_enemy->getFighter()->active_effects[eff2idx(effect.type)] = effect;
			}

			//check if opponent died
			if (m_enemy->getFighter()->health <= 0 && m_enemy->getRemainingFighters() <= 1) stransition(ENEMY_DIED_STATE);

			//check if opponent needs to switch fighters
			if (m_enemy->getFighter()->health <= 0) stransition(ENEMY_FORCED_SWITCH_FIGHTER_STATE);


			stransition(ENEMY_EFFECT_STATE);
		});
		addTransition(ENEMY_EFFECT_STATE, [&](state) -> ytho
		{

			//check applicable effects
			for (auto& effect : m_enemy->getFighter()->active_effects)
			{
				if (effect.duration == 0) continue;
				switch (effect.type)
				{

				//poison effect
				case pixl_sf::game::Effect::POISON:
					m_enemy->getFighter()->health -= effect.strength * effect.duration;
					effect.duration--;
					break;

				//fire effect
				case pixl_sf::game::Effect::FIRE:
					m_enemy->getFighter()->health -= effect.strength;
					effect.duration--;
					break;
				default:break;
				}
			}


			
			//check if opponent died
			if (m_enemy->getFighter()->health <= 0 && m_enemy->getRemainingFighters() <= 1) stransition(ENEMY_DIED_STATE);

			//check if opponent needs to switch fighters
			if (m_enemy->getFighter()->health <= 0) stransition(ENEMY_FORCED_SWITCH_FIGHTER_STATE);

			int chance = distribution(device);

			enum Action
			{
				ATTACK,
				SWITCH_FIGHTER,
				DO_NOTHING
			} current_action = ATTACK;


			//depending on the difficulty recalculate your chances for ... everything;
			switch(m_difficulty_ref)
			{
			case 0:
				if (chance < 30) current_action = ATTACK;
				else if (chance >= 30 && chance < 60) current_action = SWITCH_FIGHTER;
				else current_action = DO_NOTHING;
				break;
			case 1:
				if (chance < 45) current_action = ATTACK;
				else if (chance >= 45 && chance < 75) current_action = SWITCH_FIGHTER;
				else current_action = DO_NOTHING;
				break;

			case 2:
				if (chance < 60) current_action = ATTACK;
				else if (chance >= 60 && chance < 92) current_action = SWITCH_FIGHTER;
				else current_action = DO_NOTHING;
				break;

			case 3:
				if (chance < 70) current_action = ATTACK;
				else if (chance >= 70 && chance < 95) current_action = SWITCH_FIGHTER;
				else current_action = DO_NOTHING;
				break;
			default: current_action = ATTACK; break;
			}
			
			if (current_action == ATTACK) stransition(ENEMY_RANDOM_CHOOSE_ATTACK_STATE);
			if (current_action == SWITCH_FIGHTER) stransition(ENEMY_RANDOM_SWITCH_FIGHTER_STATE);
			if (current_action == DO_NOTHING) stransition(PLAYER_EFFECT_STATE);

			throw std::logic_error("How did you get here ?");
			
			sstay();
			
		});
		addTransition(ENEMY_RANDOM_CHOOSE_ATTACK_STATE, [&](state) -> ytho
		{
			static int counter = 0;

			//make the enemy "fake think"
			if(counter != 200)
			{
				counter++;
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				sstay();
			}
			
			counter = 0;
			
			std::uniform_int_distribution<size_t> d (0, m_enemy->getFighter()->attacks.size() - 1);
			const size_t attack_idx = d(device);

			//select a random attack from the current fighter
			m_enemy_attack = m_enemy->getFighter()->attacks[attack_idx];
			stransition(PLAYER_TAKE_DAMAGE_STATE);
		});
		addTransition(ENEMY_FORCED_SWITCH_FIGHTER_STATE, [&](state) -> ytho
		{
			m_player_score++;
			stransition(ENEMY_RANDOM_CHOOSE_ATTACK_STATE);
		});
		addTransition(ENEMY_RANDOM_SWITCH_FIGHTER_STATE, [&](state pre) -> ytho
		{
			std::uniform_int_distribution<size_t> d(0, m_enemy->getRemainingFighters() - 1);

			//select a random fighter
			m_enemy->selectFighter(d(device));

			if(pre != ENEMY_FORCED_SWITCH_FIGHTER_STATE)
				stransition(PLAYER_CHOOSE_ATTACK_STATE);
			stransition(ENEMY_RANDOM_CHOOSE_ATTACK_STATE);
		});
		addTransition(PLAYER_TAKE_DAMAGE_STATE, [&](state) -> ytho
		{
			//do damage calculation
			int weakness_mod = 0;

			//extract weakness
			auto& weakness = m_player->getFighter()->active_effects[eff2idx(pixl_sf::game::Effect::WEAKNESS)];

			//check if weakness exists
			if (weakness.type != pixl_sf::game::Effect::NONE || weakness.duration != 0) {
				weakness_mod = weakness.strength;
				weakness.duration--;
			}

			m_status_text.setString("Enemy attacked  with: " + std::to_string(m_enemy_attack.attack_power * (weakness_mod+1)) + " damage");

			//reduce health
			m_player->getFighter()->health -= m_enemy_attack.attack_power * (weakness_mod + 1);

			//add effects
			for (const auto& effect : m_enemy_attack.effects)
			{
				if (effect.type != pixl_sf::game::Effect::NONE)
					m_player->getFighter()->active_effects[eff2idx(effect.type)] = effect;
			}

			//check if opponent died
			if (m_player->getFighter()->health <= 0 && m_player->getRemainingFighters() <= 1) stransition(PLAYER_DIED_STATE);

			//check if opponent needs to switch fighters
			if (m_player->getFighter()->health <= 0) stransition(PLAYER_FORCED_CHOOSE_FIGHTER_STATE);

			
			stransition(PLAYER_EFFECT_STATE);
		});
		addTransition(PLAYER_EFFECT_STATE, [&](state)->ytho
		{
			//check applicable effects
			for (auto& effect : m_player->getFighter()->active_effects)
			{
				if (effect.duration == 0) continue;
				switch (effect.type)
				{

					//poison effect
				case pixl_sf::game::Effect::POISON:
					m_player->getFighter()->health -= effect.strength * effect.duration;
					effect.duration--;
					break;

					//fire effect
				case pixl_sf::game::Effect::FIRE:
					m_player->getFighter()->health -= effect.strength;
					effect.duration--;
					break;
				default:break;
				}
			}



			//check if player died
			if (m_player->getFighter()->health <= 0 && m_player->getRemainingFighters() <= 1) stransition(PLAYER_DIED_STATE);

			//check if opponent needs to switch fighters
			if (m_player->getFighter()->health <= 0) stransition(PLAYER_FORCED_CHOOSE_FIGHTER_STATE);
			stransition(PLAYER_CHOOSE_ATTACK_STATE);
		});
		addTransition(PLAYER_FORCED_CHOOSE_FIGHTER_STATE, [](state)->ytho
		{
			stransition(PLAYER_CHOOSE_FIGHTER_STATE);
		});
		addTransition(PLAYER_CHOOSE_FIGHTER_STATE, [&](state pre)->ytho
		{
			//check if we should transition back the attack-selection
			if (!m_choose_fighter_button_pressed && !(pre == PLAYER_FORCED_CHOOSE_FIGHTER_STATE)) stransition(PLAYER_CHOOSE_ATTACK_STATE);


			//check if a button was pressed
			auto buttons = listenButtonPress();

			//setup the button texts
			for (auto& b : m_buttons)
			{
				b->setText("");
			}
			for (size_t i = 0; i < std::min(m_player->getRemainingFighters(), m_buttons.size()); ++i)
			{
				m_buttons[i]->setText(m_player->getNameOfFighter(i));
			}


			//check if the selected fighter is valid and select it
			for (size_t i = 0; i < std::min(m_player->getRemainingFighters(), buttons.size()); ++i)
			{
				if(buttons[i])
				{
					m_player->selectFighter(i);
				
					//if the player was previously forced to choose a fighter he may now attack
					if (pre == PLAYER_FORCED_CHOOSE_FIGHTER_STATE)
						stransition(PLAYER_CHOOSE_ATTACK_STATE);
					stransition(ENEMY_RANDOM_CHOOSE_ATTACK_STATE);
				}
			}
			sstay();
		});
		addTransition(PLAYER_DIED_STATE, [&](state)->ytho
		{
			m_status_text.setString("You died - Game Over");
			for(auto& button : m_buttons)
			{
				button->setText("QUIT");
			}
			auto button_presses = listenButtonPress();

			if(*std::max_element(button_presses.begin(), button_presses.end()))
			{
				m_someone_died = true;
				stransition(PLAYER_CHOOSE_ATTACK_STATE);
			}
			sstay();
		});
		addTransition(ENEMY_DIED_STATE, [&](state)->ytho
		{
			m_status_text.setString("Enemy died - Game Over");
			for (auto& button : m_buttons)
			{
				button->setText("QUIT");
			}
			auto button_presses = listenButtonPress();

			if (*std::max_element(button_presses.begin(), button_presses.end()))
			{
				m_someone_died = true;
				m_player_score++;
				stransition(PLAYER_CHOOSE_ATTACK_STATE);
			}
			sstay();
		});
	}

	//check if any of the players died
	[[nodiscard]] bool is_gameOver() const
	{
		return m_someone_died;
	}

	//reset the state-machine
	void reset()
	{
		m_someone_died = false;
	}

	
	//get the final score for the player
	int getScore() const
	{
		return m_player_score;
	}
	enum States : transition_id_t
	{
		PLAYER_CHOOSE_ATTACK_STATE,
		PLAYER_FORCED_CHOOSE_FIGHTER_STATE,
		PLAYER_CHOOSE_FIGHTER_STATE,
		PLAYER_ATTACK_STATE,
		PLAYER_TAKE_DAMAGE_STATE,
		PLAYER_EFFECT_STATE,
		ENEMY_EFFECT_STATE,
		ENEMY_RANDOM_CHOOSE_ATTACK_STATE,
		ENEMY_RANDOM_SWITCH_FIGHTER_STATE,
		ENEMY_FORCED_SWITCH_FIGHTER_STATE,
		ENEMY_TAKE_DAMAGE_STATE,
		ENEMY_DIED_STATE,
		PLAYER_DIED_STATE
	};

	void draw(sf::RenderWindow* wnd) override
	{
		//once trick
		static auto once = [&] {

			//get position of the window
			auto [x, y] = wnd->getSize();
			const sf::Vector2f windowSize = { static_cast<float>(x),static_cast<float>(y) };
			const sf::Vector2f offset = {
				windowSize.x / 2 - BUTTON_OFFS.x * 2.5f,
				500
			};

			//set the position of the buttons
			for (size_t i = 0, j = 0; i < m_buttons.size(); ++i)
			{
				//check in which row to put the button
				if (!(i % 2))j++;
				m_buttons[i]->setPosition(sf::Vector2f{ BUTTON_OFFS.x * static_cast<float>(j),i % 2 * BUTTON_OFFS.y } + offset);
			}

			return true;
			
		}();

		//make sure the once was actually executed
		assert(once);

		wnd->draw(m_status_text);
		
	}
	
private:


	sf::Text m_status_text;
	
	const int& m_difficulty_ref;
	bool m_someone_died = false;
	
	int m_player_score = 0;
	
	bool m_choose_fighter_button_pressed = false;
	
	pixl_sf::game::GameActor* m_player,* m_enemy;
	pixl_sf::game::Attack m_player_attack, m_enemy_attack;
	
	std::array<std::shared_ptr<pixl_sf::ui::Button>, 6> m_buttons;

	pixl_sf::ui::ButtonManager* m_button_manager;
	std::array<bool, 6> m_button_presses{ false };

	std::mt19937 device;

	const sf::Vector2f BUTTON_OFFS{ 250,90 };
	const sf::Vector2f BUTTON_SIZE{ 200,60 };

	//check if any of the buttons was pressed 
	std::array<bool, 6> listenButtonPress()
	{
		std::array<bool, 6> ret{ false };
		std::swap(m_button_presses, ret);
		return ret;
	}
};

void player_setup(pixl_sf::game::GameActor* player,sf::RenderWindow* window);
void enemy_setup(pixl_sf::game::GameActor* enemy,sf::RenderWindow* window);



int main(int argc,char ** argv)
{
	argh::parser cmdl(argc, argv);
	int w{1280}, h{720};
	std::string sz_str = "";

	int difficulty = 0;
	

	//parse window size
	if(cmdl({"--size","-s"}) >> sz_str)
	{
		if(sscanf_s(sz_str.c_str(), "%dx%d", &w, &h) != 2)
		{
			w = 1280;
			h = 720;
		}
		
	}

	//parse fullscreen switch
	bool fs = cmdl[{"--fullscreen", "-fs"}];

	//parse clear-leaderboards switch
	if(cmdl[{"--clear-leaderboard","-cl"}])
	{
		remove(LEADERBOARD_FILE);
	}

	//parse default-leaderboard switch
	if(cmdl[{"---build-default-leaderboard" , "-bl"}])
	{
		std::string contents = R"({"scores":[]})";
		std::ofstream ofs(LEADERBOARD_FILE);
		ofs << contents;
		ofs.close();
	}

	//get the player name
	std::string player_name;
	if(!(cmdl({"--player_name","-n"})>>player_name))
	{
		player_name = "Player";
	}

	if(!cmdl({"--default-difficulty", "-d"})>> difficulty)
	{
		if(difficulty > 4 ||difficulty < 0)
		{
			difficulty = 0;
		}
	}

	
	//return if headless
	if (cmdl[{"--headless", "--nohead", "-q"}]) return 0;
	
	using namespace pixl_sf;

	//create rendering environment
	sf::RenderWindow window(sf::VideoMode(w, h), "Window", fs ? sf::Style::Fullscreen : sf::Style::Default);
	game::SceneManager scene_manager;

	//create and push scenes
	game::Scene* menuScene = scene_manager.addScene("MenuScene");
	game::Scene* gameScene = scene_manager.addScene("GameScene");
	scene_manager.pushScene("GameScene");
	scene_manager.pushScene("MenuScene");


	//create player & enemy
	auto player = gameScene->addGameObject<game::GameActor>();
	auto enemy = gameScene->addGameObject<game::GameActor>();

	//create button-manager & state-machine
	auto buttonManager = gameScene->addGameObject<ui::ButtonManager>();
	auto stateMachine = gameScene->addGameObject<GameStateMachine>(player, enemy, buttonManager, difficulty);

	//configure the start-game button
	auto start_button = menuScene->addGameObject<ui::Button>();
	start_button->setText("Go to Game!");
	start_button->setSize({ 200,100 });
	start_button->setPosition({ 0,0 });
	start_button->onEvent([&](auto e, auto) {
		if (e == pixl_sf::ui::Button::ON_PRESS) {
			stateMachine->reset();
			scene_manager.popScene();
			player_setup(player,&window);
			enemy_setup(enemy,&window);
		}
	});

	
	
	//configure the end game button
	auto end_button = menuScene->addGameObject <pixl_sf::ui::Button>();
	end_button->setText("End Game");
	end_button->setSize({ 200,100 });
	end_button->setPosition({ 0,100 });
	end_button->onEvent([](auto e, auto) {
		if (e == pixl_sf::ui::Button::ON_PRESS) std::exit(0);
	});


	//Create a Button to delete the leader-board file
	//this may cause inability to save the default leaderboard
	auto erase_data_button = menuScene->addGameObject<ui::Button>();
	erase_data_button->setText("Erase Leaderboard");
	erase_data_button->setSize({ 200,100 });
	erase_data_button->setPosition({ 0,200 });
	erase_data_button->onEvent([](auto e, auto)
	{
		if (e == pixl_sf::ui::Button::ON_PRESS)
			if(remove(LEADERBOARD_FILE))
			{
				throw std::runtime_error("Could not delete leaderboard: " + std::to_string(errno));
			}
	});

	auto set_diffuclty_button = menuScene->addGameObject<ui::Button>();
	set_diffuclty_button->setText("Difficulty: " + std::to_string(difficulty));
	set_diffuclty_button->setSize({ 200,100 });
	set_diffuclty_button->setPosition({ 0,300 });
	set_diffuclty_button->onEvent([&](auto e, auto* btn)
	{
		if(e == pixl_sf::ui::Button::ON_PRESS)
		{
			if (++difficulty > 4)
			{
				difficulty = 0;
			}
			btn->setText("Difficulty: " + std::to_string(difficulty));
		}
	});
	


	

	//load highscore
	using json = nlohmann::json;
	std::ifstream hs_file(LEADERBOARD_FILE);
	json high_score;
	if (hs_file.good())
	{
		hs_file >> high_score;
		hs_file.close();
	}


	//create the Highscore element from a dynamic gameobject
	auto dyn_obj = menuScene->addGameObject < pixl_sf::game::DynamicGameObject>();
	dyn_obj->setDraw([&](auto* wnd) {

		if (high_score["scores"].is_array())
		{

			int i = 0;
			for (auto score : high_score["scores"])
			{
				sf::Text t((std::string("Name: ") + score["name"].get<std::string>() + " Score: " + std::to_string(score["score"].get<int>())).c_str(), pixl_sf::resources::ResourceManager::getDefaultFont());
				t.setPosition({ 300.f, 30.f * (float)++i });
				t.setFillColor({ 0,0,0 });
				wnd->draw(t);
			}
		}
	});
	while(window.isOpen())
	{

		//poll events
		for(sf::Event evnt{}; window.pollEvent(evnt);)
		{
			switch (evnt.type)
			{
				case sf::Event::Closed: window.close();
				default: break;
			}
		}

		//update
		end_button->onMouseEvent(sf::Mouse::getPosition(window), sf::Mouse::isButtonPressed(sf::Mouse::Left));
		start_button->onMouseEvent(sf::Mouse::getPosition(window), sf::Mouse::isButtonPressed(sf::Mouse::Left));
		erase_data_button->onMouseEvent(sf::Mouse::getPosition(window), sf::Mouse::isButtonPressed(sf::Mouse::Left));
		set_diffuclty_button->onMouseEvent(sf::Mouse::getPosition(window), sf::Mouse::isButtonPressed(sf::Mouse::Left));
		scene_manager.update(&window);

		//check if game is over
		if(scene_manager.is_active("GameScene") && stateMachine->is_gameOver())
		{
			scene_manager.pushScene("MenuScene");
			
			//extract score
			json::array_t a = high_score["scores"];

			//assemble entry
			auto entry = json::object({ {"name",player_name},{"score",stateMachine->getScore()} });

			auto found_score = std::find_if(a.begin(), a.end(), [player_name](auto obj)
			{
				return obj["name"].template get <std::string>() == player_name;
			});
			if(found_score != a.end())
			{
				*found_score = entry;
			}
			else {
				a.push_back(entry);
			}
			
			//write score
			high_score["scores"] = a;
			std::ofstream ofs(LEADERBOARD_FILE);
			ofs << std::setw(4) << high_score << std::endl;
			
		}
		

		//render
		window.clear({255,255,255});
		scene_manager.draw(&window);
		window.display();
	}
}


void player_setup(pixl_sf::game::GameActor* player,sf::RenderWindow* window)
{
	(void)window;
	player->clear();

	
	//configure player
	player->setName("Player");
	player->setLayoutInverted(true);
	player->setDrawPos({ 0,0 });

	
	for (int i = 0; i < 1; ++i) {
		auto fighter = player->addFighter(pixl_sf::game::FighterFactory::loadFighter("fighters/fire_monster.json"));
		if (i == 3 || i == 2)
		{
			fighter->health = 60;
		}

	}

	player->addFighter(pixl_sf::game::FighterFactory::loadFighter("fighters/rainbow_monster.json"));
	
	player->selectFighter(3);


}
void enemy_setup(pixl_sf::game::GameActor* enemy,sf::RenderWindow * window)
{
	enemy->clear();
	//configure enemy
	enemy->setName("Enemy");
	enemy->setDrawPos({ window->getSize().x / 2.f,0 });
	for (int i = 0; i < 10; ++i) {
		enemy->addFighter(pixl_sf::game::FighterFactory::loadFighter("fighters/fire_monster.json"));
	}
}
